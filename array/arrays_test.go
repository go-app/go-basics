package array

import (
	"testing"
)

func TestArrays(t *testing.T) {
	var numbers [5]int

	expectedLen := 5
	if len(numbers) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(numbers), expectedLen)
	}

	grades := [...]int{12, 14, 33}
	expectedLen = 3
	if len(grades) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(grades), expectedLen)
	}

	for index := 0; index < len(grades); index++ {
		grades[index]++
		t.Log(grades[index])
	}

	for _, grade := range grades {
		t.Log(grade)
	}

	var matrix [3][1]int = [3][1]int{{100}, {200}, {300}}
	t.Log(matrix)

	expectedLen = 3
	if len(matrix) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(matrix), expectedLen)
	}
	expectedLen = 1
	for _, line := range matrix {
		if len(line) != expectedLen {
			t.Errorf("Size is %v, want: %v.", len(line), expectedLen)
		}
	}

	var matrix2 [3][2]int
	matrix2[0] = [...]int{5, 3}
	matrix2[1] = [...]int{55, 3}
	matrix2[2] = [...]int{555, 3}
	t.Log(matrix2)

	expectedLen = 3
	if len(matrix2) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(matrix), expectedLen)
	}
	expectedLen = 2
	for _, line := range matrix2 {
		if len(line) != expectedLen {
			t.Errorf("Size is %v, want: %v.", len(line), expectedLen)
		}
	}

	arr := [...]int{1, 2, 3}
	linkArr1 := &arr
	linkArr2 := &arr
	linkArr1[0] = 15

	if arr[0] != 15 || linkArr1[0] != 15 || linkArr2[0] != 15 {
		t.Errorf("Expect 15 on position zero  %v, %v, %v", arr[0], linkArr1[0], linkArr2[0])
	}

	otherArr := [...]int{8, 8, 8}
	copyOfOtherArr := otherArr
	copyOfOtherArr[0] = 15

	if otherArr[0] != 8 || copyOfOtherArr[0] != 15 {
		t.Errorf("Expect 8 and 15 on position zero  %v, %v", otherArr[0], copyOfOtherArr[0])
	}
}
