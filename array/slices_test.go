package array

import (
	"testing"
)

func TestSubSlices(t *testing.T) {

	// new slice with numbers one to zero
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	expectedLen := 10
	if len(a) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(a), expectedLen)
	}

	// create a link of same array as slice
	b := a[:]
	if len(b) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(b), expectedLen)
	}

	// create sub-slice
	c := a[0 : len(a)-2]
	expectedLen = 8
	if len(c) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(c), expectedLen)
	}

	// slices are like links -> all changed
	b[5] = 10
	if a[5] != 10 || b[5] != 10 || c[5] != 10 {
		t.Errorf("Expect 10 on position fivez  %v, %v, %v", a[5], b[5], c[5])
	}

	// get sub-slice
	d := c[2 : len(c)-2]
	expectedLen = 4
	if len(d) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(d), expectedLen)
	}
}
func TestSlicesAppend(t *testing.T) {

	// create slice with make
	a := make([]int, 3, 5)
	expectedLen := 3
	if len(a) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(a), expectedLen)
	}

	// create empty slice and fill it with append
	b := []int{}
	b = append(b, 1)
	b = append(b, 1)
	b = append(b, 1)
	if len(b) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(b), expectedLen)
	}

	// init new slice
	x := []int{2, 2, 2}
	// create a array-copy
	c := append(b, x...)
	expectedLen = 6
	if len(c) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(c), expectedLen)
	}
	// validate that it is a copy
	x[1] = 3
	for _, val := range c {
		if val == 3 {
			t.Errorf("Get %v ,but want 1 or 2", val)
		}
	}
	c[4] = 1
	for _, val := range x {
		if val == 1 {
			t.Errorf("Get %v ,but want 2", val)
		}
	}
}

func TestMethodCalls(t *testing.T) {
	// create slice with make
	slice := []int{1, 2, 3}
	sliceMethod(slice, 0, 20)

	if slice[0] != 20 {
		t.Errorf("Expect 20 on position 0 but get %v", slice[0])
	}

	// same with array
	array := [...]int{1, 2, 3}
	arrayMethod(array, 0, 20)

	if array[0] != 1 {
		t.Errorf("Expect 1 on position 0 but get %v", array[0])
	}
}

func sliceMethod(slice []int, index, value int) {
	// expect change on caller level because slices behave like links on arrays
	slice[index] = value
}

func arrayMethod(array [3]int, index, value int) {
	// expect no change on caller level because array is a copy
	array[index] = value
}
