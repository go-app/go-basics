package channel

import (
	"fmt"
	"sync"
	"testing"
)

var waitGroup = sync.WaitGroup{}

// send numbers between two threads
func TestPingPong(t *testing.T) {
	ch := make(chan int)

	receiveOne := 0
	receiveTwo := 0

	waitGroup.Add(2)
	go func() {
		// receive
		receiveOne = <-ch
		// send
		ch <- 27
		waitGroup.Done()
	}()
	go func() {
		// send
		ch <- 42
		// receive
		receiveTwo = <-ch
		waitGroup.Done()
	}()

	waitGroup.Wait()

	if receiveOne != 42 {
		t.Errorf("Expect 42, but got: %v.", receiveOne)
	}
	if receiveTwo != 27 {
		t.Errorf("Expect 27, but got: %v.", receiveTwo)
	}
}

// Define with parameter that a function can only receeive or only send
func TestSendOrReceiveFunction(t *testing.T) {

	ch := make(chan int)
	receiveOne := 0

	waitGroup.Add(2)
	go func(ch <-chan int) {
		// receive 42
		receiveOne = <-ch
		waitGroup.Done()
	}(ch)
	go func(ch chan<- int) {
		// send 42
		ch <- 42
		waitGroup.Done()
	}(ch)

	waitGroup.Wait()

	if receiveOne != 42 {
		t.Errorf("Expect 42, but got: %v.", receiveOne)
	}
}

// Send and receive 2 times and check order of receiving
func TestOrderMatters(t *testing.T) {
	ch := make(chan int, 10)

	receiveOne := 0
	textOne := ""
	receiveTwo := 0
	textTwo := ""

	waitGroup.Add(2)
	go func(ch <-chan int) {
		receiveOne = <-ch
		textOne = fmt.Sprint(receiveOne, " and ", receiveTwo)
		receiveTwo = <-ch
		textTwo = fmt.Sprint(receiveOne, " and ", receiveTwo)
		waitGroup.Done()
	}(ch)
	go func(ch chan<- int) {
		ch <- 42
		ch <- 27
		waitGroup.Done()
	}(ch)

	waitGroup.Wait()

	if textOne != "42 and 0" {
		t.Errorf("Expect '42 and 0' as text, but got: %v.", textOne)
	}
	if textTwo != "42 and 27" {
		t.Errorf("Expect '42 and 27' as text, but got: %v.", textTwo)
	}
}

func TestRangChannnel(t *testing.T) {
	ch := make(chan int, 10)

	count := 9
	collect := []int{}

	waitGroup.Add(2)
	go func(ch <-chan int) {
		for i := range ch {
			t.Log("Receive:", i)
			collect = append(collect, i)
		}
		waitGroup.Done()
	}(ch)
	go func(ch chan<- int) {
		for i := 0; i < count; i++ {
			msg := 42 + i
			ch <- msg
		}
		t.Log("Close Ch")
		// need to close channel to stop range in first method
		close(ch)
		waitGroup.Done()
	}(ch)

	waitGroup.Wait()

	if len(collect) != count {
		t.Errorf("Expect %v numbers, but got: %v.", count, collect)
	}
}

func TestRangChannelBreak(t *testing.T) {
	ch := make(chan int, 10)

	count := 9
	collect := []int{}

	waitGroup.Add(2)
	go func(ch <-chan int) {
		for {
			if i, ok := <-ch; ok {
				collect = append(collect, i)
			} else {
				break
			}
		}
		waitGroup.Done()
	}(ch)
	go func(ch chan<- int) {
		for i := 0; i < count; i++ {
			msg := 42 + i
			ch <- msg
		}
		t.Log("Close Ch")
		// need to close channel to stop range in first method
		close(ch)
		waitGroup.Done()
	}(ch)

	waitGroup.Wait()

	if len(collect) != count {
		t.Errorf("Expect %v numbers, but got: %v.", count, collect)
	}
}
