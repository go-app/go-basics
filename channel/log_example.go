package channel

import (
	"fmt"
	"time"
)

const (
	logI = "INFO"
	logW = "WARNING"
	logE = "Error"
)

type logEntry struct {
	time     time.Time
	severity string
	message  string
}

var logCh = make(chan logEntry, 50)
var doneCh = make(chan struct{})

func run() {

	go logger2()

	logCh <- logEntry{time.Now(), logI, "App is starting"}
	logCh <- logEntry{time.Now(), logI, "App is shutting down"}

	time.Sleep(1000 * time.Millisecond)
	doneCh <- struct{}{}
}

func logger() {
	for entry := range logCh {
		fmt.Printf("%v - [%v]%v\n", entry.time.Format("2006-01-02T15:04:05"), entry.severity, entry.message)
	}
}

func logger2() {
	for {
		select {
		case entry := <-logCh:
			fmt.Printf("%v - [%v]%v\n", entry.time.Format("2006-01-02T15:04:05"), entry.severity, entry.message)
		case <-doneCh:
			break
		}
	}
}
