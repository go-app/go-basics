package channel

import (
	"sync"
	"testing"
)

var waitingGroup = sync.WaitGroup{}

func TestLoop(t *testing.T) {

	count := 0
	max := 5

	// create channel
	ch := make(chan int)

	for j := 0; j < max; j++ {
		// inform to wait
		waitingGroup.Add(2)

		go func() {
			i := <-ch
			t.Log(i)
			count++
			waitingGroup.Done()
		}()
		go func(j int) {
			ch <- j
			waitingGroup.Done()
		}(j)
	}
	// wait for all threads
	waitingGroup.Wait()

	if count != max {
		t.Errorf("Expect %v calls, but got: %v.", max, count)
	}
}
