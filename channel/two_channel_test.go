package channel

import (
	"sync"
	"testing"
)

// wait for other threads
var wg = sync.WaitGroup{}

func TestSimpleChannel(t *testing.T) {
	check := false
	// create channel
	ch := make(chan int)
	// inform that we need to wait for 2 parallel processes
	wg.Add(2)
	go func() {
		// recive 42
		i := <-ch
		t.Log("Receive:", i)
		if i != 42 {
			t.Errorf("Expect 42, but got: %v.", i)
		}
		check = true
		wg.Done()
	}()
	go func() {
		// send 42
		ch <- 42
		t.Log("Send 42")
		wg.Done()
	}()
	// wait for both threads
	wg.Wait()

	if !check {
		t.Errorf("Did not execute correct")
	}
}
