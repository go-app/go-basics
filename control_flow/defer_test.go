package control_flow

import (
	"fmt"
	"testing"
)

type cover struct {
	name string
}

func TestDeferOrder(t *testing.T) {

	cover := new(cover)

	func() {
		t.Log(cover)
		defer changeName(cover, "1")
		defer changeName(cover, "2")
		defer changeName(cover, "3")
		t.Log(cover)
	}()

	if cover.name != "1" {
		t.Error("Expect 1, but got ", cover)
	}
}

func changeName(cover *cover, value string) {
	fmt.Println(value)
	cover.name = value
}

func TestDeferVariables(t *testing.T) {

	cover := new(cover)

	func() {
		a := "One"
		defer changeName(cover, a)
		a = "Two"
	}()

	if cover.name != "One" {
		t.Error("Expect One, but got ", cover)
	}
}
