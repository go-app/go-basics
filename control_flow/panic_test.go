package control_flow

import (
	"fmt"
	"testing"
)

func simplePanic() {
	a, b := 1, 0
	num := a / b
	fmt.Println(num)
}

func TestCatchPanic(t *testing.T) {
	defer func() {
		err := recover()
		if err == nil {
			t.Error("Panic was not catched")
		}
	}()
	panic("Bad Bad Panic")
}
