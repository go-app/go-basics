package control_flow

import (
	"testing"
)

func TestIntSwitch(t *testing.T) {
	switch car := "12"; car {
	case "audi", "12":
		t.Log("Right car", car)
	case "bmw":
		t.Error("Wrong car", car)
	}
}

func TestMultiSwitch(t *testing.T) {
	car := "audi"
	i := 12
	switch {
	case car == "audi" || car == "bmw":
		t.Log("Right car", car)
	case i == 12:
		t.Error("Right but to late...")
	}
}

func TestFallThrough(t *testing.T) {
	car := "audi"
	i := 12
	switch {
	case car == "audi" || car == "bmw":
		t.Log("Right car", car)
		fallthrough
	case i == 12:
		t.Log("Right with fallthrough", car)
		fallthrough
	case i > 12:
		t.Log("Wrong but possible with fallthrough ", i)
	}
}

func TestType(t *testing.T) {

	var i interface{}
	i = 24

	switch i.(type) {
	case int:
		t.Log("Right type int")
	case float32:
		t.Error("Wrong type float")
	case string:
		t.Error("Wrong type string")
	}
}
