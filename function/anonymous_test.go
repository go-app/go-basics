package function

import (
	"testing"
)

func TestFunctionInLoop(t *testing.T) {

	sum := 0

	for i := 0; i < 5; i++ {
		func(count int) {
			sum += count
		}(i)
	}
	if sum != 10 {
		t.Error("Expect 10, but got ", sum)
	}
}

func TestAnonymousFunction(t *testing.T) {
	f := func(count int) int {
		return count * 2
	}
	num := f(12)
	if num != 24 {
		t.Error("Expect 2*12, but got ", num)
	}
}
