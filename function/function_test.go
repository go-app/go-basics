package function

import (
	"testing"
)

func TestMethodCall(t *testing.T) {
	var bill = calculateBill(2, 5)
	if bill != 10 {
		t.Error("Expect  2*5 is 10 but got ", bill)
	}
}

func calculateBill(price int, no int) int {
	var totalPrice = price * no
	return totalPrice
}

func TestVargs(t *testing.T) {
	sum := sum(1, 2, 3, 4, 5)
	if sum != 15 {
		t.Error("Expect 15 but got ", sum)
	}
}

func sum(values ...int) int {
	sum := 0
	for _, v := range values {
		sum += v
	}
	return sum
}

type cover struct {
	number int
}

func TestPointer(t *testing.T) {

	c := cover{number: 12}

	changeCover(c)
	if c.number != 12 {
		t.Error("Expect 12 because of copy of cover in method call, got ", c)
	}

	useCoverLink(&c, 15)
	if c.number != 15 {
		t.Error("Expect 15 because of link in method call, got ", c)
	}

	mapx := map[string]int{
		"audi": 5,
	}
	changeMap(mapx)

	if mapx["nass"] != 22 {
		t.Error("Expect 'nass' = 22 because maps are always links, got ", mapx)
	}

}
func changeCover(cover cover) {
	cover.number = 33
}

func changeMap(innerMap map[string]int) {
	innerMap["nass"] = 22
}

func useCoverLink(linkC *cover, num int) {
	linkC.number = num
}
