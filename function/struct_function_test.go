package function

import (
	"testing"
)

type greeter struct {
	greeting string
	name     string
	count    int
}

func (g *greeter) greet() {
	g.count++
}

func TestGreeter(t *testing.T) {
	gree := greeter{
		greeting: "Hello",
		name:     "Bob",
	}
	gree.greet()
	gree.greet()
	gree.greet()
	gree.greet()
	gree.greet()

	if gree.count != 5 {
		t.Error("After 5 calls we  expect count == 5, but got ", gree)
	}
}
