package goroutines

import (
	"sync"
	"testing"
	"time"
)

var wg = sync.WaitGroup{}

func TestLinkParameter(t *testing.T) {
	msg := "Hello"
	wg.Add(1)
	go func(txt *string) {
		// expected 'Goodby' because of link in method-call
		if *txt != "Goodby" {
			t.Error("Expect 'Goodby', but got: ", *txt)
		}
		wg.Done()
	}(&msg)
	msg = "Goodby"
	wg.Wait()
}

func TestCopyParameter(t *testing.T) {
	msg := "Hello"
	wg.Add(1)
	go func(txt string) {
		// expected 'Hello' because of copy string in method-call
		if txt != "Hello" {
			t.Error("Expect 'Hello', but got: ", txt)
		}
		wg.Done()
	}(msg)
	msg = "Goodby"
	wg.Wait()
}

func TestUnexpectedString(t *testing.T) {
	msg := "Hello"
	go func() {
		// unexpected goodby is printed
		if msg != "Goodby" {
			t.Error("Expect 'Goodby', but got: ", msg)
		}
	}()
	msg = "Goodby"
	wait()
}

func TestMethodCall(t *testing.T) {
	go sayHello(t)
	wait()
}

func sayHello(t *testing.T) {
	t.Log("Hello")
}

func wait() {
	time.Sleep(100 * time.Millisecond)
}
