package goroutines

import (
	"sync"
	"testing"
)

var waitgroup = sync.WaitGroup{}
var counter = 0

func TestWaitGroupDone(t *testing.T) {

	for i := 0; i < 10; i++ {
		waitgroup.Add(2)
		go sayMoin()
		go increment()
	}
	waitgroup.Wait()

	if counter != 10 {
		t.Error("Expect 10 got: ", counter)
	}
}

func increment() {
	counter++
	waitgroup.Done()
}

func sayMoin() {
	waitgroup.Done()
}
