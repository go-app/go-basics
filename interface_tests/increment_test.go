package interface_tests

import (
	"testing"
)

func TestIncrement(t *testing.T) {
	myInt := new(IntCounter)
	for i := 0; i < 10; i++ {
		myInt.Increment()
	}

	if *myInt != 10 {
		t.Error("Expect 10, but got: ", myInt)
	}
}

type Incrementor interface {
	Increment() int
}

type IntCounter int

func (ic *IntCounter) Increment() int {
	*ic++
	return int(*ic)
}
