package interface_tests

import (
	"fmt"
	"testing"
)

type Writer interface {
	Write([]byte) (int, error)
}

type ConsoleWriter struct {
	content string
	count   int
}

func (cw *ConsoleWriter) Write(data []byte) (int, error) {
	cw.content = string(data)
	n, err := fmt.Println(cw.content)
	return n, err
}

func doSome(w Writer) {
	// use interface function
	w.Write([]byte("Hello Go!"))

	// try to find  specific type
	switch w.(type) {
	case *ConsoleWriter:
		cw, _ := w.(*ConsoleWriter)
		cw.count++
	}
}

func TestInterface(t *testing.T) {
	w := &ConsoleWriter{}
	doSome(w)
	doSome(w)

	if w.count != 2 {
		t.Error("Expect count 2, not: ", w.count)
	}

	if w.content != "Hello Go!" {
		t.Error("Expect 'Hello Go!', not: ", w.content)
	}
}
