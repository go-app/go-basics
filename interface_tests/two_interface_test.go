package interface_tests

import (
	"fmt"
	"strings"
	"testing"
)

type XWriter interface {
	write([]byte) (int, error)
}

type XCloser interface {
	close() error
}

type XWriterCloser interface {
	XWriter
	XCloser
}

type MyCloserWriter struct {
	buffy string
}

func (my *MyCloserWriter) write(byb []byte) (int, error) {
	my.buffy = fmt.Sprint("Write ", string(byb))
	return 42, nil
}

func (my *MyCloserWriter) close() error {
	my.buffy += fmt.Sprint(" Close")
	return nil
}

func TestTwoInterfaces(t *testing.T) {
	tmp := new(MyCloserWriter)
	tmp.write([]byte("opa"))
	tmp.close()

	t.Log(tmp.buffy)
	if !strings.Contains(tmp.buffy, "opa") || !strings.Contains(tmp.buffy, "Write") || !strings.Contains(tmp.buffy, "Close") {
		t.Error("Expect 'Write, opa and Close' in string: ", tmp.buffy)
	}
}
