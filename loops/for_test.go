package loops

import (
	"testing"
)

func TestSimpleLoop(t *testing.T) {
	count := 0
	max := 5
	for index := 0; index < max; index++ {
		count++
	}
	if count != max {
		t.Error("Wrong loop count")
	}
}

func TestTwoCounter(t *testing.T) {
	count := 0
	max := 6
	for i, j := 0, 0; i+j < max; i, j = i+1, j+1 {
		count++
	}
	if count != max/2 {
		t.Error("Wrong loop count")
	}
}

func TestAlternativeLoops(t *testing.T) {
	count := 5
	for {
		t.Log("Hello", count)
		if count > 10 {
			break
		}
		count++
	}

	for count < 15 {
		t.Log("Hello X", count)
		count++
	}

	if count != 15 {
		t.Error("Wrong loop count")
	}
}

func TestLoopJump(t *testing.T) {
	count := 5

LoopOut:
	for i := 0; i < count; i++ {
		for j := 0; j < count; j++ {
			t.Log("Hello X", i, j)
			if i+j > 3 {
				break LoopOut
			}
		}
	}
}

func TestRange(t *testing.T) {

	s := []string{"Branch", "Tree", "Car"}

	for k, v := range s {
		t.Log(k, v)
	}

	m := map[string]int{
		"Dell": 15,
		"Exit": 110,
	}

	for _, v := range m {
		t.Log(v)
	}
}
