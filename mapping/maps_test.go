package mappping

import (
	"testing"
)

func TestMapping(t *testing.T) {
	populations := map[string]int{
		"Cali": 12,
		"Auto": 386,
		"Bus":  445,
	}

	expectedLen := 3
	if len(populations) != expectedLen {
		t.Errorf("Size is %v, want: %v.", len(populations), expectedLen)
	}

	if num, ok := populations["Auto"]; !ok {
		t.Errorf("Expect 'Auto' with num %v, but got: %v.", num, populations)
	} else {
		t.Log("'Auto' has number ", num)
	}

	if num, ok := populations["Max"]; ok {
		t.Errorf("Does not expect 'Max' with %v in map %v.", num, populations)
	} else {
		t.Log("'Max' is not in map ", populations)
	}

	populations["Max"] = 999
	if num, ok := populations["Max"]; !ok {
		t.Errorf("Expect 'Max' with num %v, but got: %v.", 999, populations)
	} else {
		t.Log("'Max' is in map with number ", num)
	}

	delete(populations, "Max")
	if num, ok := populations["Max"]; ok {
		t.Errorf("Does not expect 'Max' with %v here, we deleted him. %v", num, populations)
	} else {
		t.Log("'Max' is removed from map ", populations)
	}
}
