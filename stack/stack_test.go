package stack

import (
	"testing"
)

// Stack is an empty stack ready to use.
type Stack struct {
	data []string
}

// Push adds x to the top of the stack.
func (s *Stack) Push(x string) {
	s.data = append(s.data, x)
}

// Pop removes and returns the top element of the stack.
// It’s a run-time error to call Pop on an empty stack.
func (s *Stack) Pop() string {
	n := len(s.data) - 1
	res := s.data[n]
	s.data[n] = "" // to avoid memory leak
	s.data = s.data[:n]
	return res
}

// Size returns the number of elements in the stack.
func (s *Stack) Size() int {
	return len(s.data)
}

func TestStack(t *testing.T) {
	stack := new(Stack)
	stack.Push("world!")
	stack.Push("Hello, ")

	pop := stack.Pop()
	if pop != "Hello, " {
		t.Errorf("Expect Hello, but got: %v.", pop)
	}
	pop = stack.Pop()
	if pop != "world!" {
		t.Errorf("Expect 'world!', but got: %v.", pop)
	}

	if stack.Size() > 0 {
		t.Errorf("Expect stack empty, but has size: %v.", stack.Size())
	}
}
