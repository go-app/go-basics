package struct_tests

import (
	"testing"
)

func TestAnonymous(t *testing.T) {

	doc := struct {
		num  int
		name string
	}{
		num:  12,
		name: "Jon",
	}

	if doc.name != "Jon" || doc.num != 12 {
		t.Error("Expect Jon with 12, but got ", doc)
	}
}
