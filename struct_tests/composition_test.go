package struct_tests

import (
	"testing"
)

type animal struct {
	name   string
	origin string
}

type bird struct {
	animal
	speed  float32
	canFly bool
}

func TestDirectAccess(t *testing.T) {
	kuckuck := bird{}
	kuckuck.name = "luck"
	kuckuck.origin = "texas"
	kuckuck.speed = 23.33
	kuckuck.canFly = true

	if kuckuck.animal.name != "luck" {
		t.Error("Expect name of animal is 'luck', but got ", kuckuck)
	}
}

func TestWithNames(t *testing.T) {

	pingu := bird{
		animal: animal{
			name:   "Pingu",
			origin: "Arktis",
		},
		canFly: false,
		speed:  0,
	}

	if pingu.animal.name != "Pingu" {
		t.Error("Expect name of animal is 'Pingu', but got ", pingu)
	}

	myAnimal := &pingu.animal
	myAnimal.name = "Animal"

	if pingu.animal.name != "Animal" {
		t.Error("Expect name of animal is 'Animal', but got ", pingu)
	}
}
